<%@ page import ="java.sql.*" %>
<html>
    <head>
        <title>>Ver estado actual votaci&oacute;n mejor jugador liga ACB</title>
    </head>
    <body>
        <font size=10>
        Votaci&oacute;n al mejor jugador de la liga ACB
        <hr>
        <table>
        <tr>
			<td><b>Nombre</b></td>
			<td><b>Votos</b></td>
		</tr>
        <%
			Connection con = null;
			Statement set = null;
			ResultSet rs = null;
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");

				// Con variables de entorno
				String dbHost = System.getenv().get("DATABASE_HOST");
				String dbPort = System.getenv().get("DATABASE_PORT");
				String dbName = System.getenv().get("DATABASE_NAME");
				String dbUser = System.getenv().get("DATABASE_USER");
				String dbPass = System.getenv().get("DATABASE_PASS");

				String url = dbHost + ":" + dbPort + "/" + dbName;
				
				con = DriverManager.getConnection(url, dbUser, dbPass);
			} catch (Exception e) {
				// No se ha conectado
				System.out.println("No se ha podido conectar");
				System.out.println("El error es: " + e.getMessage());
			}
			if (con != null) {
				try{ 
					set = con.createStatement();
					rs = set.executeQuery("SELECT nombre, votos FROM Jugadores");
			        while (rs.next()) {
		%>
		<tr>
			<td name="nombre"><%= rs.getString("nombre") %></td>
			<td name="votos"><%= rs.getInt("votos") %></td>

		</tr>

		<% 
				}
					rs.close();
					set.close();
				} catch (Exception e) {
					// No lee de la tabla
					System.out.println("No lee de la tabla");
					System.out.println("El error es: " + e.getMessage());
				}
				try {
					con.close();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		%>
		</table>
        <hr>
        </font>
        <br>
        <br> <a href="index.html"> Ir al comienzo</a>
    </body>
</html>
