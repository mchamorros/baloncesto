import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PruebasPhantomjsIT
{
    private static WebDriver driver=null;

    @BeforeEach
    public void setUp() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);

        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,"/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {"--web-security=no", "--ignore-ssl-errors=yes"});
        
        driver = new PhantomJSDriver(caps);
    }

    @Test
    public void tituloIndexTest()
    {
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(), "El titulo no es correcto");
        System.out.println(driver.getTitle());
    }

    @Test
    public void comprobarValoresACero()
    {
    	Map<String, Object> vars = new HashMap<String, Object>();
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.name("B3")).click();
        driver.findElement(By.linkText("Ver estado votaciones")).click();
        vars.put("textVar0", driver.findElement(By.xpath("//tr[2]/td[2]")).getText());
        vars.put("textVar1", driver.findElement(By.xpath("//tr[3]/td[2]")).getText());
        vars.put("textVar2", driver.findElement(By.xpath("//tr[4]/td[2]")).getText());
        vars.put("textVar3", driver.findElement(By.xpath("//tr[5]/td[2]")).getText());
        String textVar0 = vars.get("textVar0").toString();
        String textVar1 = vars.get("textVar1").toString();
        String textVar2 = vars.get("textVar2").toString();
        String textVar3 = vars.get("textVar3").toString();
        assertEquals("0", textVar0, "La celda primera no tiene un valor correcto.");	
        assertEquals("0", textVar1, "La celda segunda no tiene un valor correcto.");	
        assertEquals("0", textVar2, "La celda tercera no tiene un valor correcto.");	
        assertEquals("0", textVar3, "La celda cuarta no tiene un valor correcto.");	
    }

    @Test
    public void votarOtroJugador()
    {
    	Map<String, Object> vars = new HashMap<String, Object>();
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        driver.findElement(By.xpath("//html/body/form[1]/p[6]/input[1]")).click();
        driver.findElement(By.name("txtOtros")).click();
        driver.findElement(By.name("txtOtros")).sendKeys("Otro");
        driver.findElement(By.name("B1")).click();
        driver.findElement(By.linkText("Ir al comienzo")).click();
        driver.findElement(By.linkText("Ver estado votaciones")).click();
        driver.findElement(By.cssSelector("tr:nth-child(6) > td:nth-child(2)")).click();
        vars.put("textVar0", driver.findElement(By.xpath("//tr[6]/td[1]")).getText());
        vars.put("textVar1", driver.findElement(By.xpath("//tr[6]/td[2]")).getText());
        String textVar0 = vars.get("textVar0").toString();
        String textVar1 = vars.get("textVar1").toString();
        assertEquals("Otro", textVar0, "No se encontro la celda otro.");	
        assertEquals("1", textVar1, "No se encontro la celda otro.");
    }

    @AfterEach
    public void tearDown() {
        driver.close();
        driver.quit();
    }
}